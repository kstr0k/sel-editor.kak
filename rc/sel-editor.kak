decl -hidden str __k9s0ke_tmp_loader_code
def __k9s0ke_tmp_loader -params 4.. -docstring %{
  Preprocess source code, then eval it
  Args: prefix suffix code preprocessor
} %{
  edit -scratch  # WARN: won't work if called from require-module
  eval -draft -no-hooks -save-regs 'abcdefghijklmnopqrstuvwxyz"' %{
    reg dquote %arg{3}; exec '%"_d' 'p'
    eval %arg{4}
    exec '%'; set global __k9s0ke_tmp_loader_code %reg{dot}
  }
  delete-buffer
  eval "%arg{1}%opt{__k9s0ke_tmp_loader_code}%arg{2}"
} -override -hidden

__k9s0ke_tmp_loader '' '' %{
provide-module -override @E@ %{

require-module @E@-aux1
require-module @E@-aux2

decl -hidden -docstring %{
  kakscript macro-based repeat limit
  Affects max selection count, mitigates buggy behavior
} int @E@_kakscript_macrocnt_limit 9999

decl -hidden str @E@_jumpclient ''

def @E@-info -params .. -docstring %{
  Show selection ranges + contents in info box
} %{
  @E@--zip-zdot
  eval -no-hooks -save-regs 'abr' %{
    reg a ''; reg b '
'
    @E@--strpsfx %opt{@E@_retlist}
    @E@--strcat  %opt{@E@_retlist}
    info -title  Selections %reg{r}
  }
} -override

def @E@-buf-show -docstring %{
  Show selection ranges + contents in *selections* buffer
} %{
  @E@--fill-sels-buf
  buffer *selections*
} -override

def @E@-edit -docstring %{
  @E@-buf-show in toolsclient
} %{
  @E@--validate-clients-tcjc
  # set this as jumpclient, unless in toolsclient
  try %{ @E@--strne %val{client} %opt{toolsclient} } catch %{
    try %{ @E@--strne '' %opt{@E@_jumpclient} } catch %{
      set global @E@_jumpclient %val{client}
    }
  }
  @E@--fill-sels-buf
  eval -try-client %opt{toolsclient} -verbatim buffer *selections*
} -override -hidden

def @E@-buf-desc-del -docstring %{
  Delete selection-description around cursor in *selections*
} %{
  eval -draft %{
    @E@--buf-desc-sel
    exec '"_d'
  }
} -override

def @E@-buf-desc-set -docstring %{
  Set ^ to enclosing description block in *selections*
} %{
  @E@--buf-desc-sel
  exec -save-regs '' 'Z'
} -override
def @E@-buf-desc-add -docstring %{
  Add enclosing description block in *selections* to '^'
} %{
  @E@--buf-desc-sel
  eval %{
    try %{ exec -save-regs '' '<a-z>a<a-+>' } catch %{}
    exec -save-regs '' 'Z'
  }
} -override
def @E@-buf-desc-sub -docstring %{
  Remove enclosing description block of primary cursor in *selections* from '^'
} %{
  @E@-buf-desc-add  # TODO: does '<a-z>a' preserve primary?
  exec -save-regs '' '<a-space>' 'Z'
} -override

def @E@-buf-desc-only -docstring %{
  Activate selection-description around cursor in *selections*;
  disables live mode
} %{
  @E@-live-disable
  buffer *selections*
  eval -save-regs '"ab' %{
    @E@-buf-desc-ext
    reg b ''; eval -itersel %{ reg b "%reg{b}%reg{dot}" }
    exec '%"ay' 'ggjjGe"_d' '"bp'
    eval -try-client %opt{@E@_jumpclient} %{ @E@-buf2kak }
    eval -draft %{ buffer *selections*; exec 'u' }
  }
}

def @E@-text2kak -docstring %{
  Update selections based on *selections*
} %{
  @E@-live-disable
  buffer *selections*

  # registers
  #   n = replace len
  #   y = range header; then caret for original buf
  #   e = extra keys to-exec after combining selections
  #   f = filename
  #   z = selection work
  eval -draft -save-regs '"nyfez' -no-hooks %{
    @E@--buf-desc-sel

    eval -draft %{
    exec '<a-;>;' '<a-x>H'
    reg y %reg{dot}  # copy range
    }

    eval -draft %{
    exec '<a-;>' 'J'
    exec 's(^[|]|[|$])<ret>' '"_d' '<a-x><a-_>H' '"ay'
    reg n %val{selection_length}
    exec 'u'
    }

    eval -draft %{
    exec 'gg' '<a-x>H' 's@[0-9]+@[0-9]+$<ret>' '"_d'  # just filename
    exec '<a-x>H'
    reg f %reg{dot}
    exec 'u'
    }
    exec '"_d'  # selected block processed; delete
    try %{ exec '%s^[|]<ret>'; reg e '' } catch %{
      # TODO: fails for single selection (now deleted); create dummy
      exec 'ge' '"yP' '<a-x>H' 's,.+<ret>' '"_d' '<a-x>H' 'y;' 'a,<esc>' 'p<a-l>;' 'a<ret>||<ret><esc>'
      reg e ' '
    }

    eval -try-client %opt{@E@_jumpclient} %{
      @E@-buf2kak
      exec '"zZ'
      reg y "%reg{f}@%val{timestamp}@0" %reg{y}
      exec '"yz'
      exec '"_d'
      exec 'i<ret><esc>'  # '<a-h>d' # <ret> indents?
      exec '"aPh'
      exec "%reg{n}H" 'Z' ';"_d' 'z' '"z<a-z>a' %reg{e}
      @E@-edit
    }
  }
}

def @E@-live-enable -docstring %{
  Run @E@-edit continuously (when editor idle).
} %{
  try %{ @E@--nop-2_ %val{client_list} } catch %{
    fail 'need another client'
  }
  @E@--validate-clients-tcjc
  try %{ @E@--strne %opt{toolsclient} '' } catch %{
    set global toolsclient %val{client}
  }
  try %{ @E@--strne '' %opt{@E@_jumpclient}} catch %{
    set global @E@_test_slist %val{client_list}
    set -remove global @E@_test_slist %opt{toolsclient}
    eval -save-regs a %{
      @E@--arg2reg-1 a %opt{@E@_test_slist}
      set global @E@_jumpclient %reg{a}
    }
  }
  remove-hooks global @E@-hooks
  eval -try-client %opt{@E@_jumpclient} -verbatim @E@-edit
  try %{
    @E@--fail-with @E@--fail %{ @E@--streq %opt{@E@_jumpclient} %val{client} }
    @E@--live-add-update-hook
  } catch %{
    @E@--err-chk @E@--fail
    @E@--live-add-focus-hook %opt{@E@_jumpclient}
  }
} -override
def @E@--validate-client -params 1 %{
  try %{ eval "@E@--streq "%%opt{%arg{1}} ''" } catch %{
    try %{ eval -- eval -verbatim -client "%%opt{%arg{1}}" nop } catch %{
      eval "set global %arg{1} ''"
    }
  }
} -override -hidden
def @E@--validate-clients-tcjc -params 0 %{
  @E@--validate-client @E@_jumpclient
  @E@--validate-client toolsclient
  try %{ @E@--strne %opt{toolsclient} %opt{@E@_jumpclient}} catch %{
    set global @E@_jumpclient ''
  }
} -override -hidden

def @E@-live-disable -docstring %{
  Undo @E@-live-enable
} %{
  remove-hooks global @E@-hooks
} -override

def @E@-buf2kak -docstring %{
  Apply *selection* ranges to original buffer;
  does not change selection contents
} %{
  eval -try-client %opt{@E@_jumpclient} -save-regs '^"b' %{
    buffer *selections*
    exec '%"by'
    exec 'gg<a-x>' 's@[0-9]*$<ret>' 'c@0<esc>'  # avoid invalid index # TODO
    exec '%' 's^\|[^\n]*\|[\n]<ret>' '"_d'
    exec '%s^[\n]<ret>"_d'
    exec '%<a-s>_'; reg '^' %val{selections}
    exec '%"_d' '"bP"_d'  # restore
    exec z
  }
}

def @E@--buf-desc-sel -docstring %{
  Select selection-description around cursor in *selections*
} %{
  buffer *selections*
  exec '<a-l>; <a-/>^[^|]<ret>' '<a-h>; <a-l>' '?(^[|][^\n]*[|]\n)+<ret>'
} -override -hidden

def @E@-buf-desc-ext -docstring %{
  Extend selections to entire selection-descriptions in *selections*
} %{
  eval -save-regs 'z^' %{
    buffer *selections*
    eval -draft -verbatim exec '; "zZ'
    eval -itersel %{
      @E@--buf-desc-sel
      exec '"z<a-z>a' '"zZ'
    }
    exec '"zz' '<a-+>'
  }
} -override

def @E@--live-add-update-hook %{
  hook -group @E@-hooks global NormalIdle .* %{
    @E@--fill-sels-buf
  }
  hook -group @E@-hooks global FocusOut "^%val{client}$" %{
    remove-hooks global @E@-hooks
    @E@--live-add-focus-hook %val{client}
  }
} -override -hidden
def @E@--live-add-focus-hook -params 1 %{
  hook -group @E@-hooks global FocusIn "^%arg{1}$" %{
    @E@--live-add-update-hook
  }
} -override -hidden

declare-user-mode @E@-mode
def @E@-live-new -params .. %{
  new %arg{@} %{ set global toolsclient %val{client}; @E@-live-enable }
}
def @E@--ensure-buf %{
  eval -draft %{
    try %{ buffer *selections* } catch %{
      edit -scratch *selections*; set buffer filetype @E@-sels
      alias buffer :ssk @E@-buf2kak
      map buffer @E@-mode  q          ': @E@-live-disable' -docstring %{disable live(?)}
      map buffer @E@-mode  v          ': @E@-live-enable' -docstring %{enable live(?)}
      map buffer @E@-mode  <ret>      ': enter-user-mode -lock @E@-mode<ret>' -docstring %{lock @E@-mode}
      map buffer @E@-mode  '>'        ': @E@-buf2kak<ret>' -docstring %{apply all to buf}
      map buffer normal    '>'        ': @E@-buf2kak<ret>' -docstring %{apply all to buf}
      map buffer @E@-mode  '<'        ': eval -try-client %opt{@E@_jumpclient} -verbatim @E@-edit<ret>' -docstring %{recreate from buf}
      map buffer normal    '<'        ': eval -try-client %opt{@E@_jumpclient} -verbatim @E@-edit<ret>' -docstring %{recreate from buf}
      map buffer @E@-mode  =          ': @E@-buf-desc-only<ret>' -docstring %{apply selected to buf}
      map buffer normal    =          ': @E@-buf-desc-only<ret>' -docstring %{apply selected to buf}
      map buffer @E@-mode  w          ': @E@-text2kak' -docstring %{update buf text(?)}
      map buffer @E@-mode  ' '        ': @E@-buf-desc-ext<ret>' -docstring %{select enclosing}
      map buffer normal    <a-ret>    ': @E@-buf-desc-ext<ret>' -docstring %{select enclosing}
      map buffer @E@-mode  d          ': @E@-buf-desc-del<ret>' -docstring %{delete enclosing primary}
      map buffer @E@-mode  +          ': @E@-buf-desc-add<ret>' -docstring %{add enclosing to ^}
      map buffer @E@-mode  <minus>    ': @E@-buf-desc-sub<ret>' -docstring %{remove primary from ^}
      map buffer @E@-mode  Z          ': @E@-buf-desc-set<ret>' -docstring %{set ^ to enclosing}
      map buffer @E@-mode  z          ': @E@-buf-desc-set<ret>' -docstring %{restore ^; like 'z'}
      map buffer normal    <ret>      ': enter-user-mode @E@-mode<ret>' -docstring %{enter @E@-mode}
      enter-user-mode @E@-mode  # TODO: broken: draft mode, maybe from another client
    }
  }
} -override -hidden

def @E@--fill-sels-buf -params .. %{
  eval -draft -no-hooks -save-regs 'afz' %{
    exec '"zZ'
    @E@--arg2reg-1 a %reg{z}
    @E@--zip-zdot
    @E@--ensure-buf; buffer *selections*
    try %{
      exec '%"_d' '"aP' '<a-o>l'
      reg f @E@--fill-sels-iter
      @E@--foreach %opt{@E@_retlist}
      exec 'ge<a-o>'
    }
  }
} -override -hidden
def @E@--fill-sels-iter -params .. %{
  @E@--arg2reg-1 a %opt{@E@_foreach_slist}
  set -remove global @E@_foreach_slist %reg{a} # <-- HACK: process extra arg
  exec 'ge' '<a-o>l' '"cP'
  exec 'gg' '<a-s-o>h' '"aP' 'Gg' 's^<ret>i|<esc>'
  exec '<a-x>' 's$<ret>c|<ret><esc>h' '<a-x><a-_>' '"ad' 'ge"ap'
} -override -hidden

hook -group @E@-sels global WinSetOption filetype=@E@-sels %{
  add-highlighter window/@E@-sels   group
  add-highlighter window/@E@-sels/r regions
  add-highlighter window/@E@-sels/r/none default-region \
    fill comment
  add-highlighter window/@E@-sels/r/text region \
    '^[|]' '$' fill block
  add-highlighter window/@E@-sels/r/rangespec region \
    '^[^|]' '$' fill header

  hook -once -always window WinSetOption filetype=.* %{
    remove-highlighter window/@E@-sels
  }
}


### kakscript lib

decl -hidden str-list @E@_slist
decl -hidden str-list @E@_slist1
decl -hidden str-list @E@_slist2
decl -hidden str-list @E@_retlist
decl -hidden str-list @E@_test_slist

def @E@--shift-1_1 -params 1.. -docstring %{Shift arg{2}; %arg{@:3} -> %opt{%arg{1}}} %{
  eval "
  set global %arg{1} %%arg{@}
  set -remove global %arg{1} %%arg{1} %%arg{2}
  "
} -override -hidden
def @E@--shift-2_2 -params 2.. -docstring %{Shift %arg{3}} %{
  eval -save-regs 'h' %{
    reg h %arg{2}
    eval "
    set global %arg{1} %%arg{@}
    set -remove global %arg{1} %%arg{1} %%arg{1} %%arg{2} %%arg{3}
    set global %arg{1} %%reg{h} %%opt{%arg{1}}
    "
  }
} -override -hidden

def @E@--arg2reg-1   -params 2.. %{ reg %arg{1} %arg{2} } -override -hidden
def @E@--arg2reg-1_2 -params 3.. %{ reg %arg{1} %arg{2} %arg{3} } -override -hidden

def @E@--fail-with -params 2 %{
  try %arg{2} catch %{ fail %arg{1} }
} -override -hidden

def @E@--strne -params 2 %{
  set         global @E@_test_slist %arg{1}
  set -remove global @E@_test_slist %arg{2}
  @E@--nop-1_ %opt{@E@_test_slist}
} -override -hidden

def @E@--streq -params 2 %{
  set         global @E@_test_slist %arg{1}
  set -remove global @E@_test_slist %arg{2}
  @E@--nop-0_0 %opt{@E@_test_slist}
} -override -hidden

def @E@--eval-arg1 -params 2.. %{ eval %arg{1} } -override -hidden
def @E@--eval-arg2 -params 2.. %{ eval %arg{2} } -override -hidden

def @E@--if0 -params 3 %{
  try %{
    try %{ eval %arg{1} } catch %{ fail @E@--eval-arg2 }
    fail @E@--eval-arg1
  } catch %{
    %val{error} %arg{2} %arg{3}
  }
} -override -hidden

def @E@--recurse-rep -params .. %{
  try %{ eval @E@--nop-0_0 %arg{1} } catch %{
    eval %arg{2}
    @E@--recurse-rep %arg{@}
  }
} -override -hidden

def @E@--macro-rep -params .. %{
  eval def @E@--tmp-rep-lambda -params 0 "%%{
    try %%{ @E@--nop-0_0 %arg{1} } catch %%{
      %arg{2}
    }
    try %%{ @E@--nop-1_ %arg{1} } catch %%{ fail @E@--macro-rep-stop }
  }" -override -hidden
  eval -save-regs @ %{
    reg @ ': @E@--tmp-rep-lambda<ret>'
    try %{ exec "%opt{@E@_kakscript_macrocnt_limit}q"; fail %{macro-rep: shouldn't reach end} } catch %{
      try %{
        @E@--strne %val{error} @E@--macro-rep-stop
        fail %val{error}
      } catch %{}
    }
  }
} -override -hidden

def @E@--loop-1 -params .. %{
  %arg{@}
} -override -hidden
def @E@--loop-2 -params .. %{
  %arg{@}; %arg{@}
} -override -hidden
def @E@--loop-3 -params .. %{
  %arg{@}; %arg{@}; %arg{@}
} -override -hidden
def @E@--loop-9 -params .. %{
  @E@--loop-3 @E@--loop-3 %arg{@}
} -override -hidden
def @E@--loop-81 -params .. %{
  @E@--loop-9 @E@--loop-9 %arg{@}
} -override -hidden
def @E@--loop-6561 -params .. %{
  @E@--loop-81 @E@--loop-81 %arg{@}
} -override -hidden
def @E@--loop-inf -params .. %{
  @E@--loop-6561 @E@--loop-6561 %arg{@}
  @E@-loop-inf %arg{@}  # will reach recursion limit
  fail %{loop-inf: shouldn't reach}
} -override -hidden

@E@--fail-def @E@--arg-rep-stop
def @E@--arg-rep -params .. -docstring %{
  Args: list-to-iterate iterator-name iterator-code
} %{
  eval def "@E@--%arg{2}-tmp-rep-lambda" -params 0 "%%{  # TODO: runtime def
    try %%{ @E@--nop-0_0 %arg{1} } catch %%{ %arg{3} }
    try %%{ @E@--nop-1_  %arg{1} } catch %%{ fail @E@--arg-rep-stop }
  }" -override -hidden
  try %{
    @E@--loop-inf "@E@--%arg{2}-tmp-rep-lambda"
  } catch %{ @E@--err-chk @E@--arg-rep-stop }
} -override -hidden

def @E@--zip %{
  set global @E@_retlist
  eval -verbatim -save-regs 'cd' @E@--zip-aux1
} -override -hidden
def @E@--zip-aux1 -params .. %{
  @E@--arg-rep %{%opt{@E@_slist1}} zip %{
    @E@--arg2reg-1 c %opt{@E@_slist1}
    @E@--arg2reg-1 d %opt{@E@_slist2}
    set -add global @E@_retlist %reg{c} %reg{d}
    set -remove global @E@_slist1 %reg{c}
    set -remove global @E@_slist2 %reg{d}
  }
} -override -hidden

def @E@--zip-zdot %{
  eval -save-regs z %{ exec '"zZ'; @E@--shift-1_1 @E@_slist1 %reg{z} }
  set global @E@_slist2 %val{selections}
  #eval -itersel %{ set -add global @E@_slist2 %reg{.} }

  @E@--zip
} -override -hidden

### end kakscript lib

}  # end module @E@

# loader won't run nested in provide code; nest another module outside provide
__k9s0ke_tmp_loader 'provide-module @E@-aux1 %{' '}' %{
def @E@--nop-NNN_NNN -params NNN..NNN -docstring %{ fail if nargs != NNN} %{
} -override -hidden
def @E@--nop-NNN_    -params NNN..    -docstring %{ fail if nargs < NNN} %{
  nop --
} -override -hidden
def @E@--nop-_NNN    -params ..NNN    -docstring %{ fail if nargs > NNN} %{
  nop --
} -override -hidden
# end sub-module
} %{  # sub-loader processor
  set global __k9s0ke_tmp_loader_code ''
  def __k9s0ke_tmp_loader_1 -params 1 %{
    exec '%s' NNN '<ret>c' %arg{1} '<esc>'
    exec '%'; set -add global __k9s0ke_tmp_loader_code %reg{dot}; exec '%"_d' 'p'
  } -override -hidden
  __k9s0ke_tmp_loader_1 0
  __k9s0ke_tmp_loader_1 1
  __k9s0ke_tmp_loader_1 2
  __k9s0ke_tmp_loader_1 3
  reg dquote %opt{__k9s0ke_tmp_loader_code}; exec '%"_d' 'p'
}  # end sub-loader processor

# these defs are required for defining other defs:
__k9s0ke_tmp_loader 'provide-module @E@-aux2 %{' '}' %{
} %{
require-module @E@-aux1
def @E@--fail-def -params 1 %{
  def %arg{1} -params .. %{ fail %val{error} } -override -hidden
} -override -hidden
@E@--fail-def @E@--fail

decl -hidden str-list @E@_test_err_slist
def @E@--err-chk -params .. %{
  set         global @E@_test_err_slist %arg{@}
  set -remove global @E@_test_err_slist %val{error}
  %opt{@E@_test_err_slist}
} -override -hidden

decl -hidden str-list @E@_foreach_slist
# foearch = %reg{f}; c = current
def @E@--foreach -params .. %{
  set global @E@_foreach_slist %arg{@}
  eval -verbatim -save-regs c @E@--foreach-aux1
} -override -hidden
def @E@--foreach-aux1 -params .. %{
  @E@--arg-rep %{%opt{@E@_foreach_slist}} foreach %{
    @E@--arg2reg-1 c %opt{@E@_foreach_slist}
    set -remove global @E@_foreach_slist %reg{c}
    %reg{f}
  }
} -override -hidden

decl -hidden int @E@_for_cnt
decl -hidden str-list @E@_for_args
@E@--fail-def @E@--for-stop
def @E@--int-for-countdown -params 1.. %{
  set global @E@_for_cnt %arg{1}
  @E@--shift-1_1 @E@_for_args %arg{@}
  try %{
    @E@--loop-inf @E@--int-for-countdown-iter %opt{@E@_for_args}
  } catch %{ @E@--err-chk @E@--for-stop }
} -override -hidden
def @E@--int-for-countdown-iter -params .. %{
  try %{ @E@--strne %opt{@E@_for_cnt} 0 } catch %{ fail @E@--for-stop }
  %arg{@}
  set -remove global @E@_for_cnt 1
} -override -hidden

def @E@--strcat -params .. -docstring %{
  Result: %reg{r}
} %{
  eval -save-regs f %{
    reg r ''
    reg f @E@--strcat-iter
    @E@--foreach %arg{@}
  }
} -override -hidden
def @E@--strcat-iter -params .. %{
  reg r "%reg{r}%reg{c}"
} -override -hidden

def @E@--strpsfx -params .. -docstring %{
  Prefix args with %reg{a}, suffix with %reg{b}
  Result: @E@_retlist
} %{
  eval -save-regs f %{
    set global @E@_retlist
    reg f @E@--strpsfx-iter
    @E@--foreach %arg{@}
  }
} -override -hidden
def @E@--strpsfx-iter -params .. %{
  set -add global @E@_retlist "%reg{a}%reg{c}%reg{b}"
} -override -hidden

} %{}  # end module @E@-aux2, no preprocessing

} %{  # main loader processor
  exec '%s' '@E@-' '<ret>c' 'sel-editor-' '<esc>'
  exec '%s' '@E@_' '<ret>c' 'sel_editor_' '<esc>'
  exec '%s' '@E@'  '<ret>c' 'sel-editor'  '<esc>'
}  # end main loader
