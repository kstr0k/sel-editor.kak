# Kakoune selection viewer / editor

## _Show selection ranges & contents in custom buffer_

## Installation

```
plug https://gitlab.com/kstr0k/sel-editor.kak
require-module sel-editor

# suggested ':' aliases (at the ':' prompt, type ANOTHER ':')
alias global :sse sel-editor-edit
alias global :ssl  sel-editor-live-enable
alias global :ss.  sel-editor-live-disable

# aliases to help with toolsclient
def iam-client-tools %{set global toolsclient %val{client} }
def new-client-tools %{new iam-client-tools}
alias global =tc iam-client-tools
alias global +=tc new-client-tools
alias global +=ssl  sel-editor-live-new
}
```

Manual installation: clone somewhere under `autoload`, and `require-module sel-editor` in your `kakrc`.

## Usage

- invoke `sel-editor-buf-show` (or `sel-editor-edit`) from any buffer
- in the `*selections*` buffer, press `<ret>` to enter a custom user mode that shows the available keys. E.g.
  - `<space>`: select the enclosing region; `d`: delete enclosing; `>`: call sel-editor-buf2kak; `=`: call `sel-editor-buf-desc-only`; `w`: call sel-editor-text2kak
- live mode: you need two clients / windows (e.g. in tmux)
  - (if not already set up) connect a new client; optionally select the tools client (e.g. the suggested `=tc`, or `+=ssl`)
  - invoke `sel-editor-live-enable` (or suggested `:ssl`); unless pre-set, this will set the `toolsclient` to be the active one.
  - make some selections
  - go to the `*selections*` window and modify the selections. Press `<ret>` to enter the custom user-mode, or just delete / modify ranges manually.
  - call `sel-editor-buf2kak` and `sel-editor-buf-desc-only` (or use the mappings in the custom user mode) to update the original buffer
  - at any time, call `sel-editor-live-disable` from any window to stop auto-updating `*selections*` (and potentially losing work)

### Key mappings

In the `*selections*` buffer, press `<ret>` in normal mode to enter `sel-editor-mode` &mdash; it will describe the available keys. Additionally, normal mode in `*selections*` supports the following keys (feel free to define your own `sel-editor-mode` normal mappings)
- `<a-ret>`: select (as `<space>` in `sel-editor-mode`)

### Commands

- `sel-editor-info`: show an info box
- `sel-editor-buf-show`: generate and show a `*selections*` buffer for the selections in the current buffer
- `sel-editor-edit`: bring up `*selections*` in `toolsclient`; sets up the invoking client as the destination for commands that update original buffer.
- `sel-editor-live-enable` (and `-disable`): live-update the `*selections*` buffer (`NormalIdle`). Also, `sel-editor-live-new ...` start a `new ...` client, makes it a `toolsclient` and calls `live-enable`.
- `sel-editor-buf-desc-ext`: extend selections in the `*selections*` buffer to entire blocks
- `sel-editor-buf-desc-add` ( / `-remove` / `-set`): combine enclosing selections with those saved in the caret register of `*selections*`. Like normal `<a-z>`, but call `-desc-ext` first, and save after combining.
- `sel-editor-buf2kak`: in the original buffer, update selections from the descriptions in the `*selections*` buffer (ranges only)
- `sel-editor-buf-desc-only`: set the selections in the original buffer to match the selected description blocks (extend first) in the `*selections*` buffer
- `sel-editor-text2kak`: update the contents of a **single** original-buffer selection based on the text in the corresponding `*selections*` description.

## Copyright

`Alin Mr. <almr.oss@outlook.com>` / MIT license
